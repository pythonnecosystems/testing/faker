# Faker: Python은 가짜이다? <sup>[1](#footnote_1)</sup>

> *페이커란 무엇인가? Python의 Faker 라이브러리에 대한 종합 가이드이다. Faker로 가짜를 만들어 보자.*

Faker는 이름, 주소, 전화번호, 이메일 주소 등과 같은 가짜 데이터를 생성할 수 있는 Python 패키지이다. 소프트웨어 개발과 테스트에서 데이터베이스 채우기, 사용자 상호작용 시뮬레이션, 데모용 샘플 데이터 생성 등 다양한 목적으로 실제와 같은 데이터를 생성하는 데 자주 사용된다.

Faker는 직접 데이터를 생성하지 않고도 실제 정보와 유사한 무작위 데이터를 생성할 수 있는 간단하고 편리한 방법을 제공한다. 이는 테스트 또는 데모 목적으로 대규모 데이터 세트를 생성해야 할 때 특히 유용하다.

<a name="footnote_1">1</a>: [Faker: Python is Just a Fake Away!](https://medium.com/@HeCanThink/faker-python-is-just-a-fake-away-ef626a0dcf8d)를 편역한 것이다.
